<?php get_header(); ?>
		<!-- main -->
		<main id="main" role="main">
			<section class="case-study col-sm-12">
				<div class="case-title">
					<p>Lake Hawea</p>
					<p>House</p>
					<p>2012</p>
				</div>
				<div class="item full">
					<div class="img-nav-con">
						<img src="/wp-content/themes/PAC-theme/img/cs-1.jpg">
						<div class="page--float">
							<div class="page-prev">
								<div class="rotate-con">
									<p>Prev &#8212; 02</p>
								</div>
							</div>
							<div class="page-next">
								<div class="rotate-con">
									<p>Next &#8212; 04</p>
								</div>
							</div>
						</div>
					</div>
					<p>Image Title</p>
				</div>
				<div class="item full">
					<div class="img-nav-con">
						<img src="/wp-content/themes/PAC-theme/img/cs-2.jpg">
						<div class="page--float">
							<div class="page-prev">
								<div class="rotate-con">
									<p>Prev &#8212; 02</p>
								</div>
							</div>
							<div class="page-next">
								<div class="rotate-con">
									<p>Next &#8212; 04</p>
								</div>
							</div>
						</div>
					</div>
					<p>Image Title</p>
				</div>
				<div class="client-case">
					<p class="quote">Client Quote &#8212; shmotatoz Proin sed libero quam. Nulla nec augue mau ris. We like potatos, cheep as chips potato potato Proin sed libero quam. Nulla nec augue mau ris.</p>
					<p class="client">Noah Butcher &#8212; The Butcher House</p>
					<div class="page--float">
						<div class="page-prev">
							<div class="rotate-con">
								<p>Prev &#8212; 02</p>
							</div>
						</div>
						<div class="page-next">
							<div class="rotate-con">
								<p>Next &#8212; 04</p>
							</div>
						</div>
					</div>
				</div>
				<div class="half-con item">
					<div class="img-nav-con">
						<div class="half">
							<img src="/wp-content/themes/PAC-theme/img/cs-3.jpg">
							<p class="mob-title">Image Title</p>
						</div>
						<div class="half">
							<img src="/wp-content/themes/PAC-theme/img/cs-4.jpg">
							<p class="mob-title">Image Title</p>
						</div>
						<div class="page--float">
							<div class="page-prev">
								<div class="rotate-con">
									<p>Prev &#8212; 02</p>
								</div>
							</div>
							<div class="page-next">
								<div class="rotate-con">
									<p>Next &#8212; 04</p>
								</div>
							</div>
						</div>
					</div>
					<div class="half-title">
						<p>Image Title</p>
						<p>Image Title</p>
					</div>
				</div>
				<div class="item full">
					<div class="img-nav-con">
						<img src="/wp-content/themes/PAC-theme/img/cs-5.jpg">
						<div class="page--float">
							<div class="page-prev">
								<div class="rotate-con">
									<p>Prev &#8212; 02</p>
								</div>
							</div>
							<div class="page-next">
								<div class="rotate-con">
									<p>Next &#8212; 04</p>
								</div>
							</div>
						</div>
					</div>
					<p>Image Title</p>
				</div>
				<div class="case-study-info col-sm-12">
					<div class="title col-sm-push-3 col-sm-8">
						<div class="section-div"></div>
						<h2>Lake Hawea</h2>
						<h2>House</h2>
						<h2>2012</h2>
					</div>
					<div class="contrib col-1-cont col-sm-push-3 col-sm-3">
						<p>Client : Eliam Petansque</p>
						<p>Architect : Aaron Paterson</p>
						<p>Design Team : Lorem Ipsum, Lorem</p>
						<p>Build : Ipsum</p>
						<p>Credit : Individual or business</p>
						<p>Credit : webaddress.com</p>
					</div>
					<div class="case-about col-sm-push-3 col-sm-6">
						<p>We like potatos, cheep as chips potato potato shmotatoz Proin sed libero quam. Nulla nec augue mauris. We like potatos, cheep as chips potato potato shmotatoz Proin sed libero quam. Nulla nec augue mauris. We like potatos, cheep as chips potato potato shmotatoz Proin sed libero quam. Nulla nec augue mauris.</p>
						<p>We like potatos, cheep as chips potato potato shmotatoz Proin sed libero quam. Nulla nec augue mauris. . Nulla nec augue mauris. We like potatos, cheep as chips potato potato shmotatoz Proin sed libero quam. Nulla nec augue mauris. . Nulla nec augue mauris.</p>
					</div>
				</div>
			</section>
			<?php get_template_part('pagination'); ?>
		</main>
		<!-- /main -->

<?php get_footer(); ?>