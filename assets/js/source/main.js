$(document).ready(function() {
'use strict';
/*global google*/
/*global History*/
/*global WURFL*/

	//Scale images
	function scale() {
		var dataAttr,
			view = $(window).width(),
			responsive = $('img.responsive');

		if( view <= 600 ) {
			dataAttr = 'mobile';
		} else if ( view > 600 && view <= 992 ) {
			dataAttr = 'tablet';
		} else if ( view > 992 && view < 1280 ) {
			dataAttr = 'desktop';
		} else if ( view >= 1280 && window.devicePixelRatio <= 1 ) {
			dataAttr = 'desktop';
		} else if ( view >= 1280 && window.devicePixelRatio > 1 ) {
			dataAttr = 'retina';
		} else if ( view > 1800 && window.devicePixelRatio <= 1 ) {
			dataAttr = 'retina';
		}
		$('img.responsive').each(function() {
			this.src = $(this).data(dataAttr);
		});
	}
	scale();


	//Mobile Menu toggle
	function mobile() {
		if( $(window).width() < 768 ) {
			$(window).on('scroll touchmove', function(){
				var uwindow = Math.abs($(window).scrollTop());

				if ( uwindow > 200 && !$('html').hasClass('right') ){
					$('.menu-mobile').removeClass('off');
				} else {
					$('.menu-mobile').addClass('off');
				}
			});
		}
	}
	mobile();


	function resize() {
		var windowWidth = $(window).width();
		$(window).resize(function() {
			if( windowWidth !== $(window).width() ) {
				mobile();
				tablet();
				scale();
				title();

				if ($('.home-section').length > 0) {
					greaterTablet();
					if ($('.h2-1').attr('style')) {
						$('.h2-1').attr('style', function(i, style) {
							return style.replace(/margin-top[^;]+;?/g, '');
						});
					}
					if ($('.h2-2').attr('style')) {
						$('.h2-2').attr('style', function(i, style) {
							return style.replace(/margin-top[^;]+;?/g, '');
						});
					}
					initFloat();

				}
			}
		});
	}
	resize();

	$(window).load(function() {
		title();
	});

	function mobileOpen() {
		$('.menu-mobile').addClass('off');
		$('#side').removeClass('off');
		$('html').addClass('right');
		$('.wrapper').addClass('right');
		$('#maps-container').addClass('right');
	}

	function mobileClose() {
		$('.menu-mobile').removeClass('off');
		$('#side').addClass('off');
		$('html').removeClass('right');
		$('.wrapper').removeClass('right');
		$('#maps-container').removeClass('right');
	}

	$('#menu-mobile').on('click touch', function() {
		mobileOpen();
	});


	$('#mobile-close').on('click touch', function() {
		mobileClose();
	});

	function tablet() {
		if ( $(window).width() <= 1024 ) {
			$('.hide-2').addClass('clear');
			$('#thumb a').on('touch', function(e) {
				if ( !$(this).hasClass('active') ) {
					e.preventDefault();
					$('.thumbnail-con a').removeClass('active');
					$(this).addClass('active');
				} else {
					return true;
				}
			});
		}
	}
	tablet();

	function floatingNav() {
		var uwindow = Math.abs($(window).scrollTop());
		var a, b, c, d, e, f, responsivea, responsiveb;

		if ( $(window).width() > 1200 && WURFL.form_factor === 'Desktop' ) {
			a = 75;
			b = 169;
			c = 378;
			d = 245;
			e = d+5;
			f = 700;
			responsivea = $('#home-link');
			responsiveb = $('.home-right');

			if ( uwindow > a ) {
				$('#tablet-nav').addClass('open');
			} else {
				$('#tablet-nav').removeClass('open');
			}

			if ( uwindow > b) {
				$('#lineCon').addClass('float');
			} else {
				$('#lineCon').removeClass('float');
			}

			if ( uwindow > c) {
				$('#lineSmall').addClass('float');
			} else {
				$('#lineSmall').removeClass('float');
			}

			if ( uwindow > d ) {
				responsivea.addClass('active');
				$('.ph-logo').css({'opacity' : 0 });
				$('#tablet-nav').css({'background-color' :  'rgba(238, 249, 254, 0.8)'});
			} else {
				responsivea.removeClass('active');
				$('.ph-logo').css({'opacity' : 1 });
				$('#tablet-nav').css({'background-color' :  'rgba(238, 249, 254, 0)'});
			} 

			if (uwindow > e) {
				responsiveb.addClass('active');
			} else {
				responsiveb.removeClass('active');
			}

		}

	}

	function tabletNav() {
		$('#tablet-nav').css({'background-color' :  'rgba(238, 249, 254, 0.8)'});

		var f, responsivea, responsiveb,
			uwindow = $(window).scrollTop();

			responsivea = $('#tab-link');
			responsiveb = $('.tab-right');

		if ( $(window).width() > 768 && $(window).width() <= 1200 ) {
			f = 310;

		} else if ( $(window).width() > 680 && $(window).width() <= 768 ) {
			f = 254;
		}

		responsiveb.addClass('active');

		if (uwindow > f) {
			$('#tablet-nav').addClass('open');
			$('#lineSmall').addClass('float');
			$('#lineCon').addClass('float');
			responsivea.addClass('active');

		} else {
			$('#tablet-nav').removeClass('open');
			$('#lineSmall').removeClass('float');
			$('#lineCon').removeClass('float');
			responsivea.removeClass('active');
		}
	}

	function initFloat() {
		$(window).on('scroll touchmove load resize', function() {
			if( WURFL.form_factor === 'Tablet' || $(window).width() <= 1200 && $(window).width() > 680 ) {
				$('.hide-2').addClass('clear');
				tabletNav();
			} else if( $(window).width() > 1200 && WURFL.form_factor === 'Desktop' ) {
				floatingNav();
			}
			trees();
		});
	}
	initFloat();

	function trees() {
		var uwindow = Math.abs($(window).scrollTop()),
			tree, treedis, treeHeight;

		$('.tree-trans').each(function() {
			tree = $(this);
			treeHeight = tree.height();
			treedis = tree.offset().top + (treeHeight/5) - 105;
			if (uwindow > treedis) {
				tree.addClass('hide-tree');
			} else {
				tree.removeClass('hide-tree');
			}
		});

	}

	//Read more
	$('#more').on('click touch', function(event) {
		event.preventDefault();
		$('.story-inside').toggleClass('show');
		$(this).hide();
		$('#less').show();
		return false;
	});

	$('#less').on('click touch', function(event) {
		event.preventDefault();
		$('.story-inside').toggleClass('show');
		$(this).hide();
		$('#more').show();
		return false;
	});

	//Smooth Scroll
	function scroller() {
		$('a[href*=#]:not([href=#])').on('click touch', function(e) {
			var target, push, scrollOptions;
			if ( !$('#side').hasClass('off') ) {
				mobileClose();
			}
			if ( $(this).hasClass('no-ajaxy') ) {
				target = $(this.hash);
				if ( ( $(window).width() > 768 ) && ( $(window).width() <= 992 ) ) {
					push = 105;
				} else if ( $(window).width() < 768 ) {
					push = 76;
				} else if ( $(window).width() > 992 && $(window).width() < 1200 ) {
					push = 80;
				} else {
					push = 105;
				}
				scrollOptions = {
						offsetTop: push,
						duration: 1500,
						easing:'swing'
					};
				target.ScrollTo(scrollOptions);
			}
			return false;
		});
	}
	scroller();

	//Map Canvas
	function initialize() {

		var styles = [
						{
							"stylers": [
								{ "hue": "#ff5500" },
								{ "saturation": -100 }
							]
						}
					];

		var styledMap = new google.maps.StyledMapType(styles,{name: "PAC Map"});

		var mapOptions = {
							center: { lat: -36.8588643, lng: 174.7547053},
							zoom: 16,
							streetViewControl: false,
							panControl: false,
							zoomControl: false,
							mapTypeControl: false,
							scaleControl: false,
							scrollwheel: false,
							mapTypeControlOptions: {
								mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
							}
						};

		var map = new google.maps.Map(document.getElementById('maps-container'),
		mapOptions);

		var myLatLng = new google.maps.LatLng(-36.8588643,174.7547053);
		var image =  {
				url: '/wp-content/themes/PAC-theme/img/PAC-MAP-PIN.png',
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(20, 25),
				scaledSize: new google.maps.Size(45, 45)
			};

		var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				icon: image
			});

		map.mapTypes.set('map_style', styledMap);
		map.setMapTypeId('map_style');

	}
	google.maps.event.addDomListener(window, 'load', initialize);

	$(document).on('click touch', 'a.page-numbers', function(e){
		var push, projects, scrollOptions, link;

		e.preventDefault();
		projects = $('#projects');
		link = $(this).attr('href');

		if ( ( $(window).width() > 768 ) && ( $(window).width() <= 1024 ) ) {
			push = 80;
		} else if ( $(window).width() < 768 ) {
			push = 76;
		} else {
			push = 105;
		}
		scrollOptions = {
						offsetTop: push,
						duration: 1500,
						easing:'swing'
					};
		projects.ScrollTo(scrollOptions);
		$('a.page-numbers').removeClass('current');
		$(this).addClass('curent');
		projects.load(link + ' #ajax-pag');
		return false;
	});

	$(document).ajaxComplete(function() {
		scale();
		visited();
		$('body').ajaxify();
		location();
		tabletNav();

		if ( $('.home-section').length > 0 ) {
			gallery();
			galleryChange();
			slickChange();
			greaterTablet();
		}
		title();
		loaded();
	});

	function loaded() {
		if($('.home-section').length === 0) {
			$('.hide-2').hide();
		}else{
			$('.hide-2').show();
		}
	}
	loaded();


	//Thumb visited states
	function visited() {
		$('#thumb a').on('click', function(e) {
			$(this).addClass('visited');
		});
	}
	visited();

	//Globals for Visited state function
	var history = [], k;


	//visited state array functions
	function location() {
		var current = window.location.href,
			newLocation = [],
			i;

		newLocation.push(current);

		$.each(newLocation, function (i, j) {
			if ($.inArray(j, history) === -1) {
				history.push(j);
			}
		});
		for (k = history.length; k > -1; k--) {
			locationInner();
		}
		return false;
	}
	location();

	function locationInner() {
		$('#thumb a').each(function() {
			if ( ($(this).attr('href') === history[k]) && (!$(this).hasClass('visited')) ) {
				$(this).addClass('visited');
			}
		});
	}

	function title() {
		var width = $('#tablet-nav li.scroll').width();
		if ($(window).width() >= 768 ) {
			$('.sub-text p.name').css({'width': width});
			$('.sub-text p.type').css({'width': width});
		}
	}


	function strike() {
		$('#tablet-nav a.no-ajaxy, #thumb a').on('touchstart', function() {
			$(this).addClass('strike');
		});
		$('#tablet-nav a.no-ajaxy, #thumb a').on('touchend', function() {
			$(this).removeClass('strike');
		});
	}

	function strikeInit() {
		if( WURFL.form_factor === 'Tablet' ) {
			strike();
		}
	}
	strikeInit();

	$(function() {

		var $body = $('.over');
		$body.bind('scroll', function() {
			// "Disable" the horizontal scroll.
			if ($body.scrollLeft() !== 0) {
				$body.scrollLeft(0);
			}
		});

	});

	//Floating Text
	function textFloat() {
		var small = $('#main').offset().top,
			large = small - 52,
			h2a = $('.h2-1').offset().top,
			h2b = $('.h2-2').offset().top,
			totala = h2a - large + 105,
			totalb = h2b - small + 105,
			uwindow, uwindowb, scrolla, scrollb, margina, marginb;
			$(window).scroll(function() {
				uwindow = Math.abs($(window).scrollTop());

				scrolla = uwindow / totala;
				margina = -(scrolla * totala);
				$('.h2-1').css({'margin-top': margina});
				if (uwindow > totalb) {
					uwindowb = uwindow - totalb;

					scrollb = uwindowb / totalb;
					marginb = -(scrollb * totalb);

					$('.h2-2').css({'margin-top' : marginb});

					if(uwindow > 800) {
						$('.hide-2').addClass('clear');
					} else {
						$('.hide-2').removeClass('clear');
					}
				} else {
					$('.h2-2').css({'margin-top' : 0 });
				}

			});
	}



	function greaterTablet(){
		if( $(window).width() > 1024 && WURFL.form_factor === 'Desktop' && $('.home-section').length > 0) {
			textFloat();
		} else {
			return false;
		}
	}
	greaterTablet();


	$('#activate').on('click touch', function() {
		$(this).toggleClass('active');
	});

	$('.wrapper, #maps-container').on('click touch', function() {
		$('#activate').removeClass('active');
	});

	//Slick gallery functions

	function initSlick() {
		$('.init-slick').slick({
			dots: false,
			infinite: true,
			speed: 800,
			slidesToShow: 1,
			adaptiveHeight: true,
			autoplay: false
		});
	}

	function gallery() {
		$('button.slick-arrow').empty();
		$('button.slick-next').html('<div class="next-div"></div>');
		$('button.slick-prev').html('<div class="prev-div"></div>');
	}
	gallery();

	function galleryChange() {
		$('div.sub-text').removeClass('active');
		var current = $('img.slick-current').data('slick-index');

		$('div.sub-text').each(function() {
			if ( $(this).data('position') === current ) {
				$(this).addClass('active');
			}
		});
	}

	function slickChange() {
		$('.init-slick').on('afterChange', function(event, slick, currentSlide, nextSlide){
			galleryChange();
		});
	}
	slickChange();

});