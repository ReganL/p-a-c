				<?php $test_query = new WP_Query('post_type=testimonial');
					if ($test_query->have_posts()) :?>
					<section class="client-con-con">
					<?php while($test_query->have_posts()):$test_query->the_post();
						$content = get_field('content');
						$author = get_field('author');

						echo "<section class='client-con'><p class='quote'>".$content."</p><p class='client'>".$author."</p></section>";

						endwhile;?>
					</section>
				<?php endif;?>
				<section id="our-story" class="our-story col-sm-12 col-md-12">
					<?php if (get_field('story_image', 'option')): $counter = 1;?>
						<div class="story-image">
							<img class="dynamic" src="<?php echo get_template_directory_uri();?>/img/dynamic-story.jpg"/>
						<?php while(has_sub_field('story_image', 'option')):
						$img = get_sub_field('image', 'option');
						$src = $img['url'];
						$alt = $img['alt'];
						$title = $img['title'];

						$img_id = $img['id'];

						$mobile = wp_get_attachment_image_src($img_id, $size ='small');
						$tablet = wp_get_attachment_image_src($img_id, $size='medium');
						$desktop = wp_get_attachment_image_src($img_id, $size='large');

						$mobile = $mobile['0'];
						$tablet = $tablet['0'];
						$desktop = $desktop['0'];

					?>
						<div class="tree-con">
							<img class="responsive tree-trans tree<?php echo $counter;?>" data-mobile="<?php echo $mobile;?>" data-tablet="<?php echo $tablet;?>" data-desktop="<?php echo $desktop;?>" data-retina="<?php echo $src;?>" alt="<?php echo $alt;?>" data-title="<?php echo $title;?>"/>
						</div>
					<?php $counter++; endwhile;?>
					</div>
					<?php endif;?>
					<div class="section-div"></div>
					<h3>Our Story</h3>
					<?php if (get_field('section_a', 'option')):
						$section_a = get_field('section_a', 'option');
					?>
					<div class="story-inside col-1 col-sm-6 col-md-6">
						<?php echo $section_a;?>
					</div>
					<?php endif;
						if (get_field('section_a', 'option')):
						$section_b = get_field('section_b', 'option');
					?>
					<div class="story-inside col-2 col-sm-6 col-md-6">
						<?php echo $section_b;?>
					</div>
					<a href="#" id="more" class="more no-ajaxy">Read More...</a>
					<a href="#" id="less" class="less no-ajaxy">Read Less...</a>
					<?php endif;?>
				</section>
				<div class="image-contact-mob">
					<?php if (get_field('contact_image', 'option')):
						$img = get_field('contact_image', 'option');
						$src = $img['url'];
						$alt = $img['alt'];
						$title = $img['title'];

						$img_id = $img['id'];

						$mobile = wp_get_attachment_image_src($img_id, $size ='small');
						$tablet = wp_get_attachment_image_src($img_id, $size='medium');
						$desktop = wp_get_attachment_image_src($img_id, $size='large');

						$mobile = $mobile['0'];
						$tablet = $tablet['0'];
						$desktop = $desktop['0'];
					?>
					<img class="responsive" data-mobile="<?php echo $mobile;?>" data-tablet="<?php echo $tablet;?>" data-desktop="<?php echo $desktop;?>" data-retina="<?php echo $src;?>" alt="<?php echo $alt;?>" data-title="<?php echo $title;?>"/>
					<?php endif;?>
				</div>
				<section class="contact col-xs-12 col-md-12 col-lg-12">
					<div id="contact" class="section-div"></div>
					<h3>Contact</h3>
					<?php if(get_field('phone', 'option')):
						$phone = get_field('phone', 'option');
						$link = preg_replace('/\s+/', '', $phone);
					?>
					<div class="col-1-contact col-md-4 col-sm-4 col-xs-12">
						<a class="contact-link tel left" href="tel:<?php echo $link;?>"><?php echo $phone;?></a>
					</div>
					<?php endif;
						if(get_field('email', 'option')):
						$email = get_field('email', 'option');
					?>
					<div class="col-2-contact col-md-4 col-sm-4 col-xs-12">
						<a class="contact-link mail right" href="mailto:<?php echo $email;?>"><?php echo $email;?></a>
					</div>
					<?php endif;?>
					<?php if (get_field('street', 'option')):
						$street = get_field('street', 'option');
					?>
					<div class="col-1-address col-md-4 col-sm-4 col-xs-12">
						<p><?php echo $street;?></p>
					</div>
					<?php endif;
						if(get_field('post', 'option')):
							$post = get_field('post', 'option');
					?>
					<div class="col-2-address col-md-4 col-sm-4 col-xs-12">
						<p><?php echo $post;?></p>
					</div>
					<?php endif;?>
					<?php if (get_field('contact_image', 'option')):
						$img = get_field('contact_image', 'option');
						$src = $img['url'];
						$alt = $img['alt'];
						$title = $img['title'];

						$img_id = $img['id'];

						$mobile = wp_get_attachment_image_src($img_id, $size ='small');
						$tablet = wp_get_attachment_image_src($img_id, $size='medium');
						$desktop = wp_get_attachment_image_src($img_id, $size='large');

						$mobile = $mobile['0'];
						$tablet = $tablet['0'];
						$desktop = $desktop['0'];
					?>
					<div class="contact-image col-md-6">
						<img class="responsive" data-mobile="<?php echo $mobile;?>" data-tablet="<?php echo $tablet;?>" data-desktop="<?php echo $desktop;?>" data-retina="<?php echo $src;?>" alt="<?php echo $alt;?>" data-title="<?php echo $title;?>"/>
					</div>
					<?php endif;?>
				</section>

			</div><!-- /wrapper end -->

			<div id="maps-container">
			</div>

			<?php if (get_field('road_view', 'option')):
				$img = get_field('road_view', 'option');
				$src = $img['url'];
				$alt = $img['alt'];
				$title = $img['title'];

				$img_id = $img['id'];

				$mobile = wp_get_attachment_image_src($img_id, $size ='small');
				$tablet = wp_get_attachment_image_src($img_id, $size='medium');
				$desktop = wp_get_attachment_image_src($img_id, $size='large');

				$mobile = $mobile['0'];
				$tablet = $tablet['0'];
				$desktop = $desktop['0'];
			?>
			<div id="activate" class="road-view">
				<img class="responsive" data-mobile="<?php echo $mobile;?>" data-tablet="<?php echo $tablet;?>" data-desktop="<?php echo $desktop;?>" data-retina="<?php echo $src;?>" alt="<?php echo $alt;?>" data-title="<?php echo $title;?>"/>
				<div class="cover-image"></div>
			</div>
			<?php endif;?>

			<!-- wrapper -->

			<div class="copy-con wrapper">
				<?php if (get_field('copyright', 'option')):
					$copy = get_field('copyright', 'option');
				?>
				<footer class="footer" role="contentinfo">
					<p class="copyright">Copyright <span>&copy;</span> <?php echo date('Y'); ?> &#8212; <?php echo $copy;?></p>
				</footer>
				<?php endif;?>
			</div>
			<!-- /wrapper -->

		<?php wp_footer(); ?>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWIQWxB4dAfAH8TO36OUBcSEcvKynMM_0"></script>
	</body>
</html>
