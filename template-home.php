<?php /* Template Name: Home */ get_header(); ?>
		<!-- main -->
		<main id="main" role="main">
			<!-- section -->
			<div id="content">
				<section class="home-section">
					<?php if (get_field('head_1')):?>
						<h2 class="h2-1"><?php the_field('head_1');?></h2>
					<?php endif;?>
					<?php if (get_field('gallery')):?>
							<div class="hero-gallery col-xs-12 col-sm-10 col-md-10">
								<div class="init-slick">
									<?php while(has_sub_field('gallery')):
										$img = get_sub_field('image');
										$src = $img['url'];
										$alt = $img['alt'];
										$title = $img['title'];

										$img_id = $img['id'];

										$mobile = wp_get_attachment_image_src($img_id, $size ='small');
										$tablet = wp_get_attachment_image_src($img_id, $size='medium');
										$desktop = wp_get_attachment_image_src($img_id, $size='large');

										$mobilesrc = $mobile['0'];
										$tabletsrc = $tablet['0'];
										$desktopsrc = $desktop['0'];

										$width = $desktop['1'];
										$height = $desktop['2'];
												?>
									<img class="responsive hero col-sm-12" data-mobile="<?php echo $mobilesrc;?>" data-tablet="<?php echo $tabletsrc;?>" data-desktop="<?php echo $desktopsrc;?>" data-retina="<?php echo $src;?>" alt="<?php echo $alt;?>" data-title="<?php echo $title;?>" height="<?php echo $height;?>" width="<?php echo $width;?>"/>
								<?php endwhile;?>
								</div>
							</div>
					<?php endif;?>
					<?php if (get_field('gallery')):
						$i = 0;
						while(has_sub_field('gallery')):
						$name = get_sub_field('name');
						$type = get_sub_field('type');
						$year = get_sub_field('year');
						$link = get_sub_field('link');
					?>
						<a href="<?php echo $link;?>">
							<div class="sub-text <?php if($i === 0):?>active<?php endif;?>" data-position="<?php echo $i;?>">
								<p class="name"><?php echo $name;?></p>
								<p class="type"><?php echo $type;?></p>
								<p class="year"><?php echo $year;?></p>
							</div>
						</a>
					<?php $i = $i+1; endwhile; endif;?>

					<div class="h2-con">
						<img src="<?php echo get_template_directory_uri();?>/img/flex-height.jpg" alt="div Flexible"/>
						<?php if (get_field('head_1')):?>
						<h2 class="h2-2"><?php the_field('head_2');?></h2>
						<?php endif;
							if(get_field('head_3')):?>
						<div class="h2-3Con">
							<h2 class="h2-3"><?php the_field('head_3');?></h2>
						</div>
						<?php endif;?>
					</div>
					<?php if(get_field('trees')): 
						$counter = 1;
					?>
					<div class="tree-con">
						<?php while(has_sub_field('trees')):
							$img = get_sub_field('image');
							$src = $img['url'];
							$alt = $img['alt'];
							$title = $img['title'];

							$img_id = $img['id'];

							$mobile = wp_get_attachment_image_src($img_id, $size ='small');
							$tablet = wp_get_attachment_image_src($img_id, $size='medium');
							$desktop = wp_get_attachment_image_src($img_id, $size='large');

							$mobilesrc = $mobile['0'];
							$tabletsrc = $tablet['0'];
							$desktopsrc = $desktop['0'];

						?>
							<img class="responsive tree-trans tree<?php echo $counter;?>" data-mobile="<?php echo $mobilesrc;?>" data-tablet="<?php echo $tabletsrc;?>" data-desktop="<?php echo $desktopsrc;?>" data-retina="<?php echo $src;?>" alt="<?php echo $alt;?>" data-title="<?php echo $title;?>" height="<?php echo $height;?>" width="<?php echo $width;?>"/>
						<?php $counter++; endwhile;?>
					</div>
					<?php endif;?>
					<div class="rec-light"></div>
					<script>
						$(document).ready(function(){

							$('.init-slick').slick({
								dots: false,
								infinite: true,
								speed: 800,
								slidesToShow: 1,
								adaptiveHeight: true,
								swipeToSlide: true,
								autoplay: false
							});

							$('button.slick-arrow').empty();
							$('button.slick-next').html('<div class="next-div"></div>');
							$('button.slick-prev').html('<div class="prev-div"></div>');

						});
					</script>
				</section>
			</div>
			<!-- /section -->
			<?php get_template_part('pagination'); ?>
		</main>
		<!-- /main -->

<?php get_footer(); ?>