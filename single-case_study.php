<?php get_header(); ?>
		<!-- main -->
		<main id="main" role="main">
			<div id="content">
				<section class="case-study col-sm-12">
					<?php $thispost = get_post($id); $menu_order = $thispost->menu_order;
						$prev = $menu_order - 1;
						$next = $menu_order + 1;
						$prev = ($prev < 10) ? "0".$prev : $prev;
						$next = ($next < 10) ? "0".$next : $next;

						$wp_query = new WP_Query('post_type=case_study');
						$max = $wp_query->found_posts;
					?>
					<div class="case-title">
						<p><?php the_title();?></p>
						<p><?php the_field('type');?></p>
						<p><?php the_field('date');?></p>
					</div>
					<?php if(get_field('content')):
						while(has_sub_field('content')):
							$img = get_sub_field('full');
							$img_l = get_sub_field('split_left');
							$img_r = get_sub_field('split_right');

							$img_id = $img['id'];
							$img_l_id = $img_l['id'];
							$img_r_id = $img_r['id'];

							$title = $img['title'];
							$title_l = $img_l['title'];
							$title_r = $img_r['title'];

							$src = $img['url'];
							$src_l = $img_l['url'];
							$src_r = $img_r['url'];

							$alt = $img['alt'];
							$alt_l = $img_l['alt'];
							$alt_r = $img_r['alt'];

							$mobile = wp_get_attachment_image_src($img_id, $size ='small');
							$tablet = wp_get_attachment_image_src($img_id, $size='medium');
							$desktop = wp_get_attachment_image_src($img_id, $size='large');

							$mobile_l = wp_get_attachment_image_src($img_l_id, $size ='small');
							$tablet_l = wp_get_attachment_image_src($img_l_id, $size='medium');
							$desktop_l = wp_get_attachment_image_src($img_l_id, $size='large');

							$mobile_r = wp_get_attachment_image_src($img_r_id, $size ='small');
							$tablet_r = wp_get_attachment_image_src($img_r_id, $size='medium');
							$desktop_r = wp_get_attachment_image_src($img_r_id, $size='large');

							$mobile = $mobile['0'];
							$tablet = $tablet['0'];
							$desktop = $desktop['0'];

							$mobile_l = $mobile_l['0'];
							$tablet_l = $tablet_l['0'];
							$desktop_l = $desktop_l['0'];

							$mobile_r = $mobile_r['0'];
							$tablet_r = $tablet_r['0'];
							$desktop_r = $desktop_r['0'];

							$quote = get_sub_field('quote');
							$client = get_sub_field('author');

						if( ($img) || ($img_l) || ($img_r) ):
					?>
					<div class="<?php if($img):?>full<?php else:?>half-con<?php endif;?> item">
						<div class="img-nav-con">
							<?php if($img):?>
								<img class="responsive" src="<?php echo $src;?>" data-mobile="<?php echo $mobile;?>" data-tablet="<?php echo $tablet;?>" data-desktop="<?php echo $desktop;?>" data-retina="<?php echo $src;?>" alt="<?php echo $alt;?>" data-title="<?php echo $title;?>" />
							<?php endif;?>
							<?php if( ($img_l) || ($img_r) ):?>
								<div class="half">
									<img class="responsive" src="<?php echo $src_l;?>" data-mobile="<?php echo $mobile_l;?>" data-tablet="<?php echo $tablet_l;?>" data-desktop="<?php echo $desktop_l;?>" data-retina="<?php echo $src_l;?>" data-title="<?php echo $title_l;?>" alt="<?php echo $alt_l;?>"/>
									<p class="mob-title"><?php echo $title_l;?></p>
								</div>
								<div class="half">
									<img class="responsive" src="<?php echo $src_r;?>" data-mobile="<?php echo $mobile_r;?>" data-tablet="<?php echo $tablet_r;?>" data-desktop="<?php echo $desktop_r;?>" data-retina="<?php echo $src_r;?>" data-title="<?php echo $title_r;?>" alt="<?php echo $alt_r;?>"/>
									<p class="mob-title"><?php echo $title_r;?></p>
								</div>
							<?php endif;?>
							<div class="page--float">
								<?php if($prev > 0):?>
								<a href="<?php echo get_permalink(get_adjacent_post(false,'',false)); ?>">
									<div class="page-prev">
										<div class="rotate-con">
											<p>Prev &#8212; <?php echo $prev;?></p>
										</div>
									</div>
								</a>
								<?php endif;?>
								<?php if($next <= $max):?>
								<a href="<?php echo get_permalink(get_adjacent_post(false,'',true)); ?>">
									<div class="page-next">
										<div class="rotate-con">
											<p>Next &#8212; <?php echo $next;?></p>
										</div>
									</div>
								</a>
								<?php endif;?>
							</div>
						</div>
						<?php if( ($img_l) || ($img_r) ):?>
						<div class="half-title">
							<p><?php echo $title_l;?></p>
							<p><?php echo $title_r;?></p>
						</div>
						<?php else:?>
							<p><?php echo $title;?></p>
						<?php endif;?>
					</div>
					<?php elseif ($quote) :?>
					<div class="client-case">
						<p class="quote"><?php echo $quote;?></p>
						<p class="client"><?php echo $client;?></p>
						<div class="page--float">
							<a href="<?php echo get_permalink(get_adjacent_post(false,'',false)); ?>">
								<div class="page-prev">
									<div class="rotate-con">
										<p>Prev &#8212; 02</p>
									</div>
								</div>
							</a>
							<a href="<?php echo get_permalink(get_adjacent_post(false,'',true)); ?>">
								<div class="page-next">
									<div class="rotate-con">
										<p>Next &#8212; 04</p>
									</div>
								</div>
							</a>
						</div>
					</div>
					<?php endif; endwhile; endif;?>
					<div class="case-study-info col-sm-12">
						<div class="title col-sm-push-3 col-sm-8">
							<div class="section-div"></div>
							<h2><?php the_title();?></h2>
							<h2><?php the_field('type');?></h2>
							<h2><?php the_field('date');?></h2>
						</div>
						<?php if(get_field('contributors')):?>
						<div class="contrib col-1-cont col-sm-push-3 col-sm-3">
							<?php while(has_sub_field('contributors')):
								$link = get_sub_field('link');
								$role = get_sub_field('role');
								$contrib = get_sub_field('contributor');
							?>
							<p><?php echo $role." : "; if( $link == true ): echo "<a target='_blank' href='http://".$contrib."'>".$contrib."</a>"; else: echo $contrib;endif;?></p>
							<?php endwhile;?>
						</div>
						<?php endif;
							if(get_field('desc')):?>
						<div class="case-about col-sm-push-3 col-sm-6">
							<p><?php the_field('desc');?></p>
							<div class="contrib-mob">
								<?php if(get_field('contributors')):
									 while(has_sub_field('contributors')):
									$link = get_sub_field('link');
									$role = get_sub_field('role');
									$contrib = get_sub_field('contributor');
								?>
								<p><?php echo $role." : "; if( $link == true ): echo "<a target='_blank' href='http://".$contrib."'>".$contrib."</a>"; else: echo $contrib;endif;?></p>
								<?php endwhile; endif;?>
							</div>
						</div>
					<?php endif;?>
					</div>
				</section>
				<script>$('.hide-2').hide();</script>
			</div>
		</main>
		<?php get_template_part('pagination'); ?>
		<!-- /main -->

<?php get_footer(); ?>