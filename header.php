<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/PAC.png" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/PAC.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<script type="text/javascript" src="//wurfl.io/wurfl.js"></script>
		<?php wp_head(); ?>
	</head>
	<body id="body" <?php body_class(); ?>>
		<div id="skip"><a href="#content">Skip to Main Content</a></div>
			<!-- wrapper -->
			<div class="menu-mobile off visibile-sm">
				<div id="menu-mobile" class="menu-icon pull-left">
					<hr class="burger-menu">
					<hr class="burger-menu">
					<hr class="burger-menu">
				</div>
				<div class="logo-sm pull-right">
					<a href="<?php echo home_url();?>"><p><?php bloginfo('name'); ?></p></a>
				</div>
			</div><!-- / Mobile -->
			<nav id="side" class="nav off mobile" role="navigation">
				<div id="mobile-close" class="mobile-close"></div>
				<div class="nav-first"></div>
				<ul>
					<li id="menu-item-8" class="scroll">
						<div class="nav-active"></div>
						<a class="no-ajaxy" href="/#projects">Projects</a>
					</li>
					<li id="menu-item-9" class="scroll">
						<div class="nav-active"></div>
						<a class="no-ajaxy" href="/#our-story">Our Story</a>
					</li>
					<li id="menu-item-10" class="scroll">
						<div class="nav-active"></div>
						<a class="no-ajaxy" href="/#contact">Contact</a>
					</li>
				</ul>
			</nav>
			<div class="wrapper col-xs-12 col-md-12 col-lg-12">
				<!-- header -->

				<header class="header clear" role="banner">
					<nav id="tablet-nav" class="nav desktop" role="navigation">
						<ul>
							<li id="menu-item-8" class="scroll">
								<div class="nav-active"></div>
								<a class="no-ajaxy" href="/#projects">Projects</a>
							</li>
							<li id="menu-item-9" class="scroll">
								<div class="nav-active"></div>
								<a class="no-ajaxy" href="/#our-story">Our Story</a>
							</li>
							<li id="menu-item-10" class="scroll">
								<div class="nav-active"></div>
								<a class="no-ajaxy" href="/#contact">Contact</a>
							</li>
						</ul>
						<a id="home-link" class="home-link" href="<?php echo home_url('/');?>"><div class="home-right"></div></a>
						<a id="tab-link" class="tab-link" href="<?php echo home_url('/');?>"><div class="tab-right"></div></a>
					</nav>

					<div class="logo head pull-right">
						<a href="<?php echo home_url('/'); ?>">
							<div class="ph-logo"></div>
							<div class="ph-logo-mob"></div>
						</a>
						<div id="lineSmall">
							<div class="hide-2"></div>
							<div class="logo-line"></div>
						</div>
					</div>
					<div id="lineCon" class="line-con">
						<div id="lineLarge" class="md-device-line"></div>
					</div>
					<div class="hide-1"></div>
				</header>
				<!-- /header -->
