<section id="projects" class="pagination">
	<div id="ajax-pag">
		<div class="section-div"></div>
		<h3>Projects </h3>

		<?php
		$page = (get_query_var('page')) ? get_query_var('page') : 1;

		$loop = new WP_Query( array(
		'post_type' => 'case_study',
		'posts_per_page' => 9,
		'orderby'=> 'menu_order',
		'paged'=>$page
		) );

		$big = 999999999;

		$args = array(
			'base'               => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format'             => '%#%',
			'total'              => $loop->max_num_pages,
			'current'            => $page,
			'show_all'           => True,
			'end_size'           => 1,
			'mid_size'           => 2,
			'prev_next'          => False,
			'type'               => 'plain',
			'add_args'           => True,
			'before_page_number' => '— P'
		);
		$prev = $page -1;
		$next = $page + 1;

		$custom_page = paginate_links($args);
	?>
		<h3 class="pag"><?php echo $custom_page;?></h3>
		<div class="page-con pager-mob-hide col-xs-12 col-sm-12">
			<?php if ($prev !== 0):?>
			<a class="no-ajaxy page-numbers pager prev pull-left col-xs-6" href="<?php echo home_url()."/page/".$prev;?>">
				<p>Previous &#8212; P<?php echo $prev;?></p>
			</a>
			<?php endif;
				if ($next <= $loop->max_num_pages):
			?>
			<a class="no-ajaxy page-numbers pager next pull-right col-xs-6" href="<?php echo home_url() ."/page/".$next;?>">
				<p>Next &#8212; P<?php echo $next;?></p>
			</a>
			<?php endif;?>
		</div>
		<div id="thumb" class="thumbnail-con">
			<?php while( $loop->have_posts() ) : $loop->the_post();
				$thumb = get_field('featured_image');
				$type = get_field('type');
				$date = get_field('date');
				$pag = get_field('featured_image_type');

				$src = $thumb['url'];
				$alt = $thumb['alt'];
				$name = $thumb['title'];

				$thumb_id = $thumb['id'];

				$mobile = wp_get_attachment_image_src($thumb_id, $size ='small');
				$tablet = wp_get_attachment_image_src($thumb_id, $size='medium');
				$desktop = wp_get_attachment_image_src($thumb_id, $size='large');

				$mobile = $mobile['0'];
				$tablet = $tablet['0'];
				$desktop = $desktop['0'];

			?>
			<a <?php if($pag === true): echo "class='full'"; else: echo "class='half-pag'"; endif;?> href="<?php the_permalink();?>">
				<img class="responsive" data-mobile="<?php echo $mobile;?>" data-tablet="<?php echo $tablet;?>" data-desktop="<?php echo $desktop;?>" data-retina="<?php echo $src;?>" alt="<?php echo $alt;?>" data-title="<?php echo $name;?>"/>
				<div class="hover"></div>
				<p class="title"><?php the_title(); echo "</br>".$type."</br>".$date;?></p>
			</a>
			<?php endwhile;?>
		</div>
		<div class="page-con col-xs-12 col-sm-12">
			<?php if ($prev !== 0):?>
			<a class="no-ajaxy page-numbers pager prev pull-left col-xs-6" href="<?php echo home_url()."/page/".$prev;?>">
				<p>Previous &#8212; P<?php echo $prev;?></p>
			</a>
			<?php endif;
				if ($next <= $loop->max_num_pages):
			?>
			<a class="no-ajaxy page-numbers pager next pull-right col-xs-6" href="<?php echo home_url() ."/page/".$next;?>">
				<p>Next &#8212; P<?php echo $next;?></p>
			</a>
			<?php endif;
				wp_reset_postdata();
			?>
		</div>
	</div>
</section>