'use strict';

module.exports = {
	//Modify these file paths to match your desired templating methods
	//This will automatically match the gulp file to your templates
	files: {
		pluginjs: {

			// Use uncompressed versions of 3rd-pary libraries.
			// They will be compressed in production.
			// Any libraries added to /vendor must be added here.
			// If you remove a library you must remove it here too.
			vendor: [
					'assets/js/vendor/jquery-2.1.4.js',
					'assets/js/vendor/*.js',
					'assets/js/vendor/ajaxy/jquery-scrollto.js',
					'assets/js/vendor/ajaxy/jquery.history.js',
					'assets/js/vendor/ajaxy/ajaxify-html5.js'
			],
			build: 'assets/js/build',
			prod: 'app/js'
		},
		mainjs: {
			source: 'assets/js/source/main.js',
			build: 'assets/js/build',
			prod: 'app/js'
		},
		svg: {
			source: 'assets/images/svg/source',
			build: 'assets/images/svg/build',
			sprite: 'assets/images/sprite',
			prod: 'app/images'
		},
		styles: {
			source: 'assets/styles/sass',
			compiled: 'assets/styles/build',
			prod: 'app/styles'
		}
	},
	//Preferred ports to use for the local app and livereload
	ports: {
		express: '4000',
		livereload: '4002'
	}
}