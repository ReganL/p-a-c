'use strict';

var gulp = require('gulp'),
	browserify = require('browserify'),
	buffer = require('vinyl-buffer'),
	files = require('./build.config.js').files,
	ftp = require('vinyl-ftp'),
	gulpLoadPlugins = require('gulp-load-plugins'),
	$ = gulpLoadPlugins(),
	ports = require('./build.config.js').ports,
	runSequence = require('run-sequence'),
	source = require('vinyl-source-stream'),
	tinylr = require('tiny-lr')(),
	watch = require('gulp-watch');

gulp.task('express', function() {
	var express = require('express');
	var app = express();
	app.use(require('connect-livereload')({port: ports.livereload}));
	app.use(express.static(__dirname));
	app.listen(ports.express);
});

gulp.task('livereload', function() {
	tinylr.listen(ports.livereload)
});

gulp.task('open', function(){
	var options = {
		url: 'http://localhost:4000',
		app: 'google chrome'
	};
	return gulp.src('./index.php')
	.pipe($.open('', options));
});

function notifyLiveReload(event) {
	var fileName = require('path').relative(__dirname, event.path);
	tinylr.changed({
		body: {
			files: [fileName]
		}
	});
}

gulp.task('watch', function() {
	gulp.watch(files.styles.source + '/*.scss', ['styles'])
	gulp.watch(files.styles.source + '/**/*.scss', ['styles'])
	gulp.watch(files.mainjs.source, ['scripts'])
	gulp.watch(files.pluginjs.vendor, ['plugins'])
	gulp.watch(files.svg.source + '/*.svg', ['svgmin'])
	gulp.watch(files.svg.build + '/*.svg', ['pngSprite'])
	gulp.watch('*.php', notifyLiveReload)
	gulp.watch(files.styles.prod + '/style.min.css', notifyLiveReload)
	gulp.watch(files.mainjs.prod + '/*.js', notifyLiveReload)
});

gulp.task('styles', function() {
	return $.rubySass(files.styles.source + '/style.scss')
	.on('error', $.util.log)
	.pipe(gulp.dest(files.styles.compiled))
	.pipe($.autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
	.pipe($.rename({ suffix: '.min' }))
	.pipe($.minifyCss())
	.pipe(gulp.dest(files.styles.prod))
	.pipe($.notify({ message: 'Styles task complete' }))
});

gulp.task('scripts', function() {
	return gulp.src(files.mainjs.source)
	.pipe($.jshint('.jshintrc'))
	.pipe($.jshint.reporter('default'))
	.pipe($.concat('main.js'))
	.pipe(gulp.dest(files.mainjs.build))
	.pipe($.uglify())
	.pipe($.rename({ suffix: '.min' }))
	.pipe(gulp.dest(files.mainjs.prod))
	.pipe($.notify({ message: 'Scripts task complete' }))
});

gulp.task('browserify', ['scripts'], function() {
	var bundler = browserify(files.mainjs.build);

	return bundler.pipe()
	.pipe(source('main.min.js'))
	.pipe(buffer())
	.pipe($.rename({ prefix : 'bundle-' }))
	.pipe(gulp.dest(files.mainjs.prod))

});

gulp.task('plugins', function() {
	return gulp.src(files.pluginjs.vendor)
	.pipe($.concat('plugins.js'))
	.pipe(gulp.dest(files.pluginjs.build))
	.pipe($.uglify())
	.pipe($.rename({ suffix: '.min' }))
	.pipe(gulp.dest(files.pluginjs.prod))
	.pipe($.notify({ message: 'Plugins task complete' }))
	.on('error', $.util.log)
});

gulp.task('svg-clean', function() {
	return gulp.src(files.svg.build + '/*.svg', {read: 'false'})
	.pipe($.clean())
})

gulp.task('svgmin', ['svg-clean'], function() {
	return gulp.src(files.svg.source + '/*.svg')
	.pipe($.newer(files.svg.build))
	.pipe($.svgmin())
	.pipe(gulp.dest(files.svg.build))
	.on('error', $.util.log)
});

var config = {
			'mode': {
				'css': {
					'spacing': {
						'padding': 10
					},
					'layout': 'diagonal',
					'sprite': '../sprite.svg',
					'bust': false,
					'render': {
						'scss': {
							'template': 'assets/template/sprite.scss',
							'dest': '../../../' + files.styles.source + '/_sprite.scss'
						}
					}
				}
			}
		};

gulp.task('sprite', function() {
	return gulp.src(files.svg.build + '/*.svg')
	.pipe($.svgSprite(config))
	.pipe(gulp.dest(files.svg.prod))
});

gulp.task('pngSprite', ['sprite'], function() {
	return gulp.src(files.svg.prod + '/sprite.svg')
	.pipe($.svg2png())
	.pipe(gulp.dest(files.svg.prod))
	.pipe($.notify({ message: 'Sprites compiled' }))
});

//https://github.com/morris/vinyl-ftp
gulp.task( 'deploy', function() {

	var pathUpload = '/dev/wp-content/themes/PAC-theme'

	//Configure this to your ftp client

	var conn = ftp.create( {
		host:     'ftp.servage.net',
		user:     'p-a-c.ftp',
		password: '!=PAC.new.pass583',
		port: '21',
		parallel: 10,
		log: $.util.log
	} );

	var globs = [
		'app/styles/**',
		'app/js/**',
		'app/images/**',
		'img/*.jpg',
		'img/*.png',
		'img/icons/*.png',
		'screenshot.png',
		'*.php'
	];

	// using base = '.' will transfer everything to /public_html correctly

	return gulp.src( globs, { base: '.', buffer: false } )
		.pipe( conn.newer( pathUpload ) ) // only upload newer files
		.pipe( conn.dest( pathUpload ) )
		.pipe($.notify({ message: 'Assets deployed...' }));
} );

gulp.task('default', function() {
	runSequence(
		'scripts',
		'plugins',
		'svgmin',
		'pngSprite',
		'styles',
		'watch'
	);
});